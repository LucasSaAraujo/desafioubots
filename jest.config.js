module.exports = {
  clearMocks: true,
  collectCoverageFrom: ["<rootDir>/src/modules/**/*.ts"],
  modulePathIgnorePatterns: ["domain", "mocks", "mock"],
  coverageDirectory: "coverage",
  coverageProvider: "v8",
  coverageReporters: ["json", "text", "lcov", "clover"],
  testEnvironment: "node",
  transform: {
    ".+\\.ts$": "ts-jest",
  },
  moduleNameMapper: {
    "@/(.*)": "<rootDir>/src/$1",
  },
};
