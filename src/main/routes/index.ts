import { Router } from "express";
import {
  SortByAmountRouter,
  RecomendWineByHistoricRouter,
  GetHighestAmountRouter,
  loyalCustomersRouter,
} from "../../modules/challenges/infra/http/routes";

const router = Router();

router.use("/sort-amount", SortByAmountRouter);
router.use("/highest-amount", GetHighestAmountRouter);
router.use("/recomend-wine", RecomendWineByHistoricRouter);
router.use("/loyal-customers", loyalCustomersRouter);

export { router };
