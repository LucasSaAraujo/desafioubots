import { app } from "./app";

const server = app.listen(3333, () => console.log("server on 🚀"));

process.on("SIGINT", () => {
  server.close(() => console.log("server off"));
});
