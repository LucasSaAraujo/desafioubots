import { ApiAdapter } from "../../domain/adapter/api-adapter";
import axios from "axios";

const api = axios.create({
  baseURL: "http://www.mocky.io/v2",
});

export class VelasquezApi implements ApiAdapter {
  public async getAll(): Promise<any> {
    const data = await api.get("/598b16861100004905515ec7");

    const result = data.data;

    return result;
  }

  public async getClients(): Promise<any> {
    const data = await api.get("/598b16291100004705515ec5");

    const result = data.data;

    return result;
  }
}
