export * from "./get-highest-amount.routes";
export * from "./sort-by-amount.routes";
export * from "./recomend-wine-by-historic.routes";
export * from "./loyal-customers.routes";
