import { Router } from "express";
import { GetHighestAmountController } from "../controllers";

const getHighestAmountController = new GetHighestAmountController();

const GetHighestAmountRouter = Router();

GetHighestAmountRouter.get("/", getHighestAmountController.handle);

export { GetHighestAmountRouter };
