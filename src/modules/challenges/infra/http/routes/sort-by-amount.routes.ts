import { Router } from "express";
import { SortByAmountController } from "../controllers";

const sortByAmountController = new SortByAmountController();

const SortByAmountRouter = Router();

SortByAmountRouter.get("/", sortByAmountController.handle);

export { SortByAmountRouter };
