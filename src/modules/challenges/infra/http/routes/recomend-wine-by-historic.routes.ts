import { Router } from "express";
import { RecomendeWineByHistoricController } from "../controllers";

const recomendeWineByHistoricController =
  new RecomendeWineByHistoricController();

const RecomendWineByHistoricRouter = Router();

RecomendWineByHistoricRouter.get(
  "/:id",
  recomendeWineByHistoricController.handle
);

export { RecomendWineByHistoricRouter };
