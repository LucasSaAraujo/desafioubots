import { Router } from "express";
import { LoyalCustomersController } from "../controllers";

const loyalCustomersController = new LoyalCustomersController();

const loyalCustomersRouter = Router();

loyalCustomersRouter.get("/", loyalCustomersController.handle);

export { loyalCustomersRouter };
