import { Request, Response } from "express";
import { GetHighestAmountFactory } from "../../factorys/get-client-by-highest-amount-factory";

export class GetHighestAmountController {
  public async handle(req: Request, res: Response): Promise<Response> {
    const getClientByHighestAmount =
      await GetHighestAmountFactory().getHistoric();

    return res.status(200).json(getClientByHighestAmount);
  }
}
