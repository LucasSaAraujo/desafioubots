import { Request, Response } from "express";
import { LoyalCustomersFactory } from "../../factorys";

export class LoyalCustomersController {
  public async handle(req: Request, res: Response): Promise<Response> {
    const loyalCustomers = await LoyalCustomersFactory().getLoyals();

    return res.status(200).json(loyalCustomers);
  }
}
