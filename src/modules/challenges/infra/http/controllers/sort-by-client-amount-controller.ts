import { Request, Response } from "express";
import { SortByAmountFactory } from "../../factorys";

export class SortByAmountController {
  public async handle(req: Request, res: Response): Promise<Response> {
    const sortByAmount = await SortByAmountFactory().sort();

    return res.status(200).json(sortByAmount);
  }
}
