import { Request, Response } from "express";
import { RecomendeWineByHistoricFactory } from "../../factorys";

export class RecomendeWineByHistoricController {
  public async handle(req: Request, res: Response): Promise<Response> {
    const { id } = req.params;
    const recomendeWine = await RecomendeWineByHistoricFactory().recomend(id);

    return res.status(200).json(recomendeWine);
  }
}
