export * from "./get-client-by-highest-amount-factory";
export * from "./sort-by-amount-factory";
export * from "./recomend-wine-by-historic-factory";
export * from "./loyal-customers-factory";
