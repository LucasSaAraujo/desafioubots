import { VelasquezApi } from "../velasquez-api/api";
import { ApiGetHighestAmount } from "../../use-cases";

export const GetHighestAmountFactory = () => {
  const apiAdapter = new VelasquezApi();
  const apiGetHighestAmount = new ApiGetHighestAmount(apiAdapter);
  return apiGetHighestAmount;
};
