import { VelasquezApi } from "../velasquez-api/api";
import { ApiRecomendeWineByHistoric } from "../../use-cases";

export const RecomendeWineByHistoricFactory = () => {
  const apiAdapter = new VelasquezApi();
  const apiRecomendeWineByHistoric = new ApiRecomendeWineByHistoric(apiAdapter);
  return apiRecomendeWineByHistoric;
};
