import { VelasquezApi } from "../velasquez-api/api";
import { ApiSortByAmount } from "../../use-cases";

export const SortByAmountFactory = () => {
  const apiAdapter = new VelasquezApi();
  const apiSortByAmount = new ApiSortByAmount(apiAdapter);
  return apiSortByAmount;
};
