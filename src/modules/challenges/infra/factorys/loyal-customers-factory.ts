import { VelasquezApi } from "../velasquez-api/api";
import { ApiLoyalCustomers } from "../../use-cases";

export const LoyalCustomersFactory = () => {
  const apiAdapter = new VelasquezApi();
  const apiLoyalCustomers = new ApiLoyalCustomers(apiAdapter);
  return apiLoyalCustomers;
};
