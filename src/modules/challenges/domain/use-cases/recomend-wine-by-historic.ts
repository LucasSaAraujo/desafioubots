export interface RecomendeWineByHistoric {
  recomend(id: string): Promise<RecomendeWineByHistoric.result>;
}

export namespace RecomendeWineByHistoric {
  export type result = string;
}
