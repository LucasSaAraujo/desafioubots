export interface LoyalCustomers {
  getLoyals(): Promise<LoyalCustomers.result>;
}

export namespace LoyalCustomers {
  export type result = Array<{
    user_name: string;
    total_sales: number;
  }>;
}
