
export interface SortByAmount {
  sort(): Promise<SortByAmount.result>;
}

export namespace SortByAmount {
  export type result = Array<{
    name: string;
    totalSales: number;
  }>;
}
