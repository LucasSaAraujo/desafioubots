import { Historic, Client } from "../models";

export interface GetByHighestAmount {
  getHistoric(): Promise<GetByHighestAmount.result>;
}

export namespace GetByHighestAmount {
  export type result =  {
    client: Client,
    HighestValueIn2016: Historic
  };
}
