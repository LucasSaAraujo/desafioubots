import { Item } from "./Item";

export interface Historic {
  codigo: string;
  data: string;
  cliente: string;
  itens: Array<Item>;
  valorTotal: number;
}
