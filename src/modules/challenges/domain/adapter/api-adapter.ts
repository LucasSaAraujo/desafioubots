export interface ApiAdapter {
  getAll(): Promise<any>;
  getClients(): Promise<any>;
}
