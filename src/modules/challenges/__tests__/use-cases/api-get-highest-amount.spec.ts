import { ApiGetHighestAmount } from "../../use-cases";
import { ApiAdapterSpy } from "../domain/adapter/mock-api-adapter";
import { GetByHighestAmount } from "../../domain/use-cases";
import { MockHistoricParams, MockClientParams } from "../domain/mocks";

const clientParams = MockClientParams();
const historicParams = MockHistoricParams();

const mockHighestAmount = (): GetByHighestAmount.result => ({
  client: clientParams[1],
  HighestValueIn2016: historicParams[3],
});

const makeSut = () => {
  const apiAdapterSpy = new ApiAdapterSpy();
  const sut = new ApiGetHighestAmount(apiAdapterSpy);
  return {
    sut,
  };
};

describe("get highest amount", () => {
  it("should return a highest amount in 2016", async () => {
    const { sut } = makeSut();
    const params = mockHighestAmount();
    const highestAmount = await sut.getHistoric();
    expect(highestAmount).toEqual(params);
  });
});
