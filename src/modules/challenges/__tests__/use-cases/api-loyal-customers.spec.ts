import { ApiLoyalCustomers } from "../../use-cases";
import { ApiAdapterSpy } from "../domain/adapter/mock-api-adapter";

const mockLoyalCustomers = () => [
  { user_name: "Vader", total_sales: 6 },
  { user_name: "Cade Bane", total_sales: 1 },
  { user_name: "Luke", total_sales: 0 },
  { user_name: "R2-D2", total_sales: 0 },
  { user_name: "Yoda", total_sales: 0 },
];

const makeSut = () => {
  const apiAdapterSpy = new ApiAdapterSpy();
  const sut = new ApiLoyalCustomers(apiAdapterSpy);
  return {
    sut,
  };
};

describe("loyal customers", () => {
  it("should return a loyal customers", async () => {
    const { sut } = makeSut();
    const params = mockLoyalCustomers();
    const loyals = await sut.getLoyals();
    expect(loyals).toEqual(params);
  });
});
