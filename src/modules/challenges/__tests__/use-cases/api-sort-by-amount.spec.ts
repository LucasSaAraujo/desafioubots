import { ApiSortByAmount } from "../../use-cases";
import { SortByAmount } from "../../domain/use-cases";
import { ApiAdapterSpy } from "../domain/adapter/mock-api-adapter";

const mockSortHistoricsByAmount = (): SortByAmount.result => [
  { name: "Vader", totalSales: 2754 },
  { name: "Cade Bane", totalSales: 298 },
  { name: "Luke", totalSales: 0 },
  { name: "R2-D2", totalSales: 0 },
  { name: "Yoda", totalSales: 0 },
];

const makeSut = () => {
  const apiAdapterSpy = new ApiAdapterSpy();
  const sut = new ApiSortByAmount(apiAdapterSpy);
  return {
    sut,
  };
};

describe("sort by amount", () => {
  it("should sort historics by amount", async () => {
    const { sut } = makeSut();
    const params = mockSortHistoricsByAmount();
    const sorted = await sut.sort();
    expect(sorted).toEqual(params);
  });
});
