import { ApiRecomendeWineByHistoric } from "../../use-cases";
import { ApiAdapterSpy } from "../domain/adapter/mock-api-adapter";

const makeSut = () => {
  const apiAdapterSpy = new ApiAdapterSpy();
  const sut = new ApiRecomendeWineByHistoric(apiAdapterSpy);
  return {
    sut,
  };
};

describe("recomend wine by historic", () => {
  it("should recomend a wine", async () => {
    const { sut } = makeSut();
    const wineRecomended = await sut.recomend("2");
    expect(wineRecomended).toEqual("olá Vader, recomendo o vinho, vinho3");
  });
});
