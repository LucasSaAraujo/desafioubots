import { ApiAdapter } from "../../../domain/adapter/api-adapter";
import { MockClientParams } from "../mocks/mock-client-params";
import { MockHistoricParams } from "../mocks/mock-historic.params";

export class ApiAdapterSpy implements ApiAdapter {
  public async getClients(): Promise<any> {
    const result = MockClientParams();
    return result;
  }

  public async getAll(): Promise<any> {
    const result = MockHistoricParams();
    return result;
  }
}
