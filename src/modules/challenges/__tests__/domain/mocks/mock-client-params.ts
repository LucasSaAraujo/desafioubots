import { Client } from "modules/challenges/domain/models";

export const MockClientParams = (): Array<Client> => [
  {
    id: 1,
    cpf: "000.000.000-01",
    nome: "Luke",
  },
  {
    id: 2,
    cpf: "000.000.000-02",
    nome: "Vader",
  },
  {
    id: 3,
    cpf: "000.000.000-03",
    nome: "R2-D2",
  },
  {
    id: 4,
    cpf: "000.000.000-04",
    nome: "Yoda",
  },
  {
    id: 5,
    cpf: "000.000.000-05",
    nome: "Cade Bane",
  },
];
