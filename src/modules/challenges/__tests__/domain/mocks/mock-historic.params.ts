import { Historic } from "modules/challenges/domain/models";
import { MockItemParams } from "./mock-item-params";

const params = MockItemParams();

export const MockHistoricParams = (): Array<Historic> => [
  {
    codigo: "38ef8665-61df-4402-94f8-3c9b28ab7123",
    data: "17-09-2016",
    cliente: "0000.000.000.05",
    itens: [params[0]],
    valorTotal: 298,
  },
  {
    codigo: "38ef8665-61df-4402-94f8-3c9b28ab7123",
    data: "17-09-2014",
    cliente: "0000.000.000.02",
    itens: [params[2], params[4]],
    valorTotal: 828,
  },
  {
    codigo: "38ef8665-61df-4402-94f8-3c9b28ab7123",
    data: "17-09-2014",
    cliente: "0000.000.000.02",
    itens: [params[0], params[4]],
    valorTotal: 326,
  },
  {
    codigo: "38ef8665-61df-4402-94f8-3c9b28ab7123",
    data: "17-09-2016",
    cliente: "0000.000.000.02",
    itens: [params[2], params[2]],
    valorTotal: 1600,
  },
];
