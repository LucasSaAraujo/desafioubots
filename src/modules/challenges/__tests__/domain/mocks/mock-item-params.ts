import { Item } from "modules/challenges/domain/models";

export const MockItemParams = (): Array<Item> => [
  {
    produto: "vinho1",
    variedade: "Merlot",
    pais: "França",
    categoria: "Tinto",
    safra: "2009",
    preco: 298,
  },
  {
    produto: "vinho2",
    variedade: "Merlot",
    pais: "França",
    categoria: "Tinto",
    safra: "2009",
    preco: 300,
  },
  {
    produto: "vinho3",
    variedade: "Merlot",
    pais: "França",
    categoria: "Tinto",
    safra: "2009",
    preco: 800,
  },
  {
    produto: "vinho4",
    variedade: "Merlot",
    pais: "França",
    categoria: "Tinto",
    safra: "2009",
    preco: 227,
  },
  {
    produto: "vinho5",
    variedade: "Merlot",
    pais: "França",
    categoria: "Tinto",
    safra: "2009",
    preco: 28,
  },
];
