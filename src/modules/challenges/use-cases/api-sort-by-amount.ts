import { SortByAmount } from "../domain/use-cases";
import { ApiAdapter } from "../domain/adapter/api-adapter";
import { Historic, Client } from "../domain/models";

interface ClientsTotalSales {
  name: string;
  totalSales: number;
}

export class ApiSortByAmount implements SortByAmount {
  constructor(private apiAdapter: ApiAdapter) {}

  public async sort(): Promise<SortByAmount.result> {
    const historics = (await this.apiAdapter.getAll()) as Historic[];
    const clients = (await this.apiAdapter.getClients()) as Client[];

    const clientsTotalSales: ClientsTotalSales[] = [];

    clients.map((client) => {
      let count = 0;
      let userCpf = `0${client.cpf.replace("-", ".")}`;
      historics.map((historic) => {
        if (userCpf === historic.cliente) {
          count += historic.valorTotal;
        }
      });
      clientsTotalSales.push({
        name: client.nome,
        totalSales: Number(count.toFixed(0)),
      });
    });

    const sortedValues = clientsTotalSales.sort(
      (a: ClientsTotalSales, b: ClientsTotalSales): number => {
        if (a.totalSales > b.totalSales) return -1;
        else true;
      }
    );

    return sortedValues;
  }
}
