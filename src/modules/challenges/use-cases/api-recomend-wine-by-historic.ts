import { RecomendeWineByHistoric } from "../domain/use-cases";
import { ApiAdapter } from "../domain/adapter/api-adapter";
import { Historic, Item, Client } from "../domain/models";

interface RecomendedWineList {
  wine_name: string;
  count: number;
}

export class ApiRecomendeWineByHistoric implements RecomendeWineByHistoric {
  constructor(private apiAdapter: ApiAdapter) {}

  public async recomend(id: string): Promise<RecomendeWineByHistoric.result> {
    const products = (await this.apiAdapter.getAll()) as Historic[];
    const clients = (await this.apiAdapter.getClients()) as Array<Client>;

    const user_id = Number(id);

    const user = clients.find((client) => {
      return client.id == user_id;
    });

    let userItems: Historic[] = [];
    const userCpf = `0${user.cpf.replace("-", ".")}`;

    products.map((product) => {
      if (product.cliente === userCpf) {
        userItems.push(product);
      }
    });

    let wineNames: string[] = [];
    let wines: Item[] = [];

    userItems.map((item) => {
      item.itens.map((wine) => {
        wines.push(wine);
        if (!wineNames.includes(wine.produto)) {
          wineNames.push(wine.produto);
        }
      });
    });

    let wineInfos = [];

    wineNames.map((wine_name) => {
      let count = 0;
      wines.map((wine) => {
        if (wine.produto === wine_name) {
          count++;
        }
      });
      wineInfos.push({ wine_name, count });
    });

    const recomended = wineInfos.sort(
      (a: RecomendedWineList, b: RecomendedWineList): number => {
        if (a.count > b.count) return -1;
        else true;
      }
    ) as RecomendedWineList[];

    const recomendedWines = `olá ${user.nome}, recomendo o vinho, ${recomended[0].wine_name}`;

    return recomendedWines;
  }
}
