import { LoyalCustomers } from "../domain/use-cases";
import { ApiAdapter } from "../domain/adapter/api-adapter";
import { Historic, Client } from "../domain/models";

interface LoyalCostumer {
  user_name: string;
  total_sales: number;
}

export class ApiLoyalCustomers implements LoyalCustomers {
  constructor(private apiAdapter: ApiAdapter) {}

  public async getLoyals(): Promise<LoyalCustomers.result> {
    const historic = (await this.apiAdapter.getAll()) as Historic[];
    const clients = (await this.apiAdapter.getClients()) as Client[];

    let users = [];

    clients.map((client) => {
      let total_sales = 0;
      let userCpf = `0${client.cpf.replace("-", ".")}`;
      historic.map((sale) => {
        if (sale.cliente === userCpf) {
          total_sales += sale.itens.length;
        }
      });
      users.push({
        user_name: client.nome,
        total_sales,
      });
    });

    const loyals = users.sort((a: LoyalCostumer, b: LoyalCostumer): number => {
      if (a.total_sales > b.total_sales) return -1;
      else true;
    });

    return loyals;
  }
}
