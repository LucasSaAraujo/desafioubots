import { GetByHighestAmount } from "../domain/use-cases";
import { ApiAdapter } from "../domain/adapter/api-adapter";
import { Historic, Client } from "../domain/models";

export class ApiGetHighestAmount implements GetByHighestAmount {
  constructor(private apiAdapter: ApiAdapter) {}

  public async getHistoric(): Promise<GetByHighestAmount.result> {
    const historics = (await this.apiAdapter.getAll()) as Array<Historic>;
    const clients = (await this.apiAdapter.getClients()) as Array<Client>;

    const ItemsIn2016: Historic[] = [];
    historics.map((item) => {
      const [, , year] = item.data.split("-");
      if (year === "2016") {
        ItemsIn2016.push(item);
      }
    });

    const value = ItemsIn2016.sort((a: Historic, b: Historic): number => {
      if (a.valorTotal > b.valorTotal) return -1;
      else true;
    });

    const HighestValueIn2016 = value[0];
    let clientWithHighest: Client;

    clients.map((client) => {
      let userCpf = `0${client.cpf.replace("-", ".")}`;
      if (userCpf === HighestValueIn2016.cliente) {
        clientWithHighest = client;
      }
    });

    return {
      client: clientWithHighest,
      HighestValueIn2016: HighestValueIn2016,
    };
  }
}
