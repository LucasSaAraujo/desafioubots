# Velasquez API

## Instruções:

### com docker:

rode o comando docker-compose up, o servidor sera iniciado na porta 3333

### Localmente:

rode o comando yarn start, um comando prestart irá deletar a pasta node_modules, e instalar todas as dependecias, e iniciar o servidor.

## Desafios:

- [x]  Listar os clientes ordenados pelo maior valor total em compras

    rotas: 

    /sort-amount

- [x]  Mostre o cliente com a maior compra unica no ultimo ano (2016)

    rotas: 

    /highest-amount

- [x]  Liste os clientes mais fieis

    rotas: 

    /loyal-customers

- [x]  Recomende um vinho para um determinado cliente a partir do histórico

    rotas: 

    /recomend-wine

## Tecnologias usadas e o motivo:

- node v16.1.0
- express v4.17.1 (criação de servidor, e facilidade ao usar os métodos http)
- axios v0.21.1 (requisições a apis externas)
- jest v27.0.3 (realização de testes)
- ts-node-dev v1.1.6 (compilar código ts)
- typescript v4.3.4 (tipagem forte, e recursos que aumentam a produtividade)